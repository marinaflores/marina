class CreateHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :histories do |t|
      t.string :name
      t.references :project, foreign_key: true
      t.string :status
      t.references :person, foreign_key: true
      t.text :description
      t.datetime :started_at
      t.datetime :finished_at
      t.datetime :deadline
      t.integer :points

      t.timestamps
    end
  end
end
