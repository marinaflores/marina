class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.text :description
      t.references :history, foreign_key: true
      t.boolean :done

      t.timestamps
    end
  end
end
