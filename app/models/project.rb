class Project < ApplicationRecord
 	belongs_to :person
	has_many :histories

  	validates :name, presence: true
  	validates :person, presence: true
end
