class Task < ApplicationRecord
  belongs_to :history
  
  validates :history, presence: true
end
