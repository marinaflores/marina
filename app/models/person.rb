class Person < ApplicationRecord
	has_many :projects
	has_many :histories

	validates :name, presence: true
	validates :email, presence: true
	validates :role, presence: true
end
