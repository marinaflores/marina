class History < ApplicationRecord
  belongs_to :project
  belongs_to :person
  has_many :tasks

  accepts_nested_attributes_for :tasks, :allow_destroy => true,
                        :reject_if => proc { |attributes| attributes['description'].blank? }
  
  validates :name, presence: true
  validates :status, presence: true
  validates :person, presence: true
  validate :validationDate

  validates_format_of :points, :with => /1[3]|[1,2,3,5,8]$|/

  
	def validationDate
		if self.finished_at < self.started_at
			errors.add(:finished_at, "menor que a Data de Início")
		end 
   
    end
end
