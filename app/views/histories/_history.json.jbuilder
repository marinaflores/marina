json.extract! history, :id, :name, :project_id, :status, :person_id, :description, :started_at, :finished_at, :deadline, :points, :created_at, :updated_at
json.url history_url(history, format: :json)
